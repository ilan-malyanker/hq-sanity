import os
import time
import requests
from requests.auth import HTTPBasicAuth

from utils import *
from test_helper import *


MAGNIFIER_API_USER = 'lightcyber'

MAGNIFIER_INFO_OUT = "MagnifierInfo.out"
BBR_STATUS_LOG = "bbr_status_log"
SERVICES_STATUS_LOG = "services_status.log"
SERVICES_COMPARE1 = "services_compare1.log"
SERVICES_COMPARE2 = "services_compare2.log"
INGESTER_SANITY_LOG = "kafka_progress_and_threads.log"

def collect_data(host, user, passwd, config):
    exec_dir = os.path.join(config["general"]["exec_dir"])
    ssh_client = get_ssh_client(host, user, passwd)

    magnifier_info_cmd = "date && less /etc/lc.conf | grep -E 'CLIENT_ID' && zgrep GIT_TAG /opt/lc/bin/lc_version.py | grep jenkins | awk '{print $3}'"
    magnifier_info_out = os.path.join(exec_dir, MAGNIFIER_INFO_OUT)
    execute_remote_command(ssh_client, magnifier_info_cmd, magnifier_info_out)
    get_version(MAGNIFIER_INFO_OUT, config)     # Also setting magnifier_version key to the current config # test commit

    # For test:  "Check BBR Status" on TestRail
    bbr_status_cmd = "zgrep 'Finished' /data/logs/base_block_registration.log | grep 'Registering tick'"
    bbr_status_out = os.path.join(exec_dir, BBR_STATUS_LOG)
    execute_remote_command(ssh_client, bbr_status_cmd, bbr_status_out)
    # End of section "Check BBR Status"

    # For test:  "Check the services are up and running"
    services_status_cmd = "/opt/lc/bin/lc_status | grep -v ' active ' | grep -v 'edl.service'"
    services_status_out = os.path.join(exec_dir, SERVICES_STATUS_LOG)
    execute_remote_command(ssh_client, services_status_cmd, services_status_out)

    services_status_compare_cmd = "/opt/lc/bin/lc_status | grep -v edl.service"
    services_compare1 = os.path.join(exec_dir, SERVICES_COMPARE1)
    services_compare2 = os.path.join(exec_dir, SERVICES_COMPARE2)
    execute_remote_command(ssh_client, services_status_compare_cmd, services_compare1)
    time.sleep(10)
    execute_remote_command(ssh_client, services_status_compare_cmd, services_compare2)
    # End of section "Check the services are up and running"

    # test ingester sanity"
    ingester_sanity_cmd = "sudo -E python /home/lc/system/current/tools/common/magnifier_ingester_sanity.py -c /etc/pan-platform/config.json -i"
    ingester_sanity_out = os.path.join(exec_dir, INGESTER_SANITY_LOG)
    execute_remote_command(ssh_client, ingester_sanity_cmd, ingester_sanity_out)
    # End of collecting data for ingester sanity

    try:
        ssh_client.close()
    except:
        pass

