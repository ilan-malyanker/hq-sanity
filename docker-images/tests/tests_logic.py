from utils import *


BBR_LAST_CLOSE_THRESHOLD = 11


def check_bbr_last_close(bbr_log_file, date_str):
    if is_empty_file(bbr_log_file): return False

    line = get_file_last_line(bbr_log_file)
    try:
        fields = line.split()
        last_tick_close = fields[0].strip().replace("/", "-") + " " + fields[1].strip()
        current_time = get_current_formatted_date_time(date_str)

        delta = get_date_diff_in_minutes(current_time, last_tick_close)
        print "Actual delta between current time and last registration time (in minutes):  " + str(delta)
        if delta < BBR_LAST_CLOSE_THRESHOLD:
            return True
    except Exception as e:
        print ("EXCEPTION in line-  " + line)
        print (str(e))

    print ("The violation is in line-  " + line)
    return False


def bbr_check_all_tiks_close_time(bbr_log_file):

    with open(bbr_log_file) as f:
        current_line = f.readline().strip(' \t\n\r')
        line_ok = bbr_check_tick_close_time(current_line)
        if not line_ok:
            print "First violation line:"
            print current_line
            return False

    return True

def bbr_check_tick_close_time(line):
    close_time = curr_time = ""

    try:
        fields = line.split()
        curr_time = fields[0].strip().replace("/", "-") + " " + fields[1].strip()
        close_time = fields[len(fields) - 2].strip() + " " + fields[len(fields) - 1].strip()

        minutes_diff = get_date_diff_in_minutes(curr_time, close_time)
        print "Printing minutes diff:" + str(minutes_diff)

        if (minutes_diff >= 40) and (minutes_diff < 42):
            return True
    except Exception as e:
        print ("EXCEPTION in line-  " + line)
        print (str(e))

    print ("The line with the violation-  " + line)
    print "Comparing close time " + str(close_time) + " with current time " + str(curr_time)
    return False
