import os
import logging
import paramiko
import optparse
import json
import time
import difflib
import operator
import datetime as parent_datetime
from datetime import datetime, timedelta
from errno import ENOENT

SSH_PORT = 22
SUCCESS_MESSAGE = "PASS"
FAILURE_MESSAGE = "FAIL"

def write_to_file(location, content):
    f = open(location, "wb")
    f.write(content)
    f.close()


def get_validated_log(conf, file_name):
    full_log_name = os.path.join(conf["general"]["exec_dir"], file_name)

    if not os.path.isfile(full_log_name):
        raise IOError(ENOENT, "File doesn't exit.", file_name)

    print "Validating log:  " + full_log_name
    return full_log_name


def is_empty_file(file_name):
    result = os.stat(file_name).st_size == 0
    if not result:
        print "File:  " + file_name + " is NOT empty!"

    return result


def create_execution_dir():
    """
    Create destination directory where all execution products would be places in
    """
    new_dir_name = "Output_%s" % time.strftime("%d-%m-%Y_%H-%M-%S")
    # logging.info("Creating execution directory named:  " + new_dir_name)
    print "Creating execution directory named:  " + new_dir_name

    full_name = os.path.join(os.path.dirname(__file__), new_dir_name)

    if not os.path.exists(full_name):
        os.makedirs(full_name)

    return full_name


def get_ssh_client(host, user, passwd):
    new_client = paramiko.SSHClient()
    new_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    new_client.load_system_host_keys()

    try:
        new_client.connect(host, SSH_PORT, user, passwd)
    except Exception as e:
        logging.error(e.message)
        return None

    return new_client


def execute_remote_command(ssh_client, command, output_log):
    try:
        stdin, stdout, stderr = ssh_client.exec_command(command)
        write_to_file(output_log, stdout.read())
    except:
        pass


def get_file_last_line(file_name):
    f1 = open(file_name, "r")
    # last_line = f1.readlines()[-1].strip().lstrip()
    last_line = f1.readlines()[-1].strip(' \t\n\r')
    print "LAST LINE:  " + last_line
    f1.close()

    return last_line


def get_file_first_line(file_name):
    f1 = open(file_name, "r")
    first_line = f1.readline()
    f1.close()

    return first_line


def get_line_with_string(file_name, string_to_find):
    with open(file_name) as f:
        for line in f:
            if string_to_find in line:
                return line


def count_lines_in_file(file_name):
    """
    For a reasonably sized file (not too large)
    :param file_name: [str] Full file name including it's path
    :return:
    """
    return len(open(file_name).readlines())


def load_config(json_file):
    """
    Loading json based config file
    :param json_file: The name of the file (without the full path)
    :return:
    """

    full_name = os.path.join(os.path.dirname(__file__), json_file)
    print "Loading configuration file:  " + full_name

    if (json_file is None) or (not os.path.exists(full_name)):       # Check input
        raise ValueError("Wrong input file !!")

    with open(full_name) as data_file:
        conf = json.load(data_file)

        return conf


def get_arg_value(arg_expr):
    return str(arg_expr).split("=")[1]


def check_time_before_nine(magnifier_file):
    line = get_file_first_line(magnifier_file)
    hours =(line.split(" ")[3]).split(":")[0]

    if int(hours) < 9:
        return True

    return False


def get_n_line_from_file(file_to_open, line_num):
    with open(file_to_open) as f:
        for i in range(1, line_num):
            f.readline()

        requested_line = f.readline()
        return requested_line


def get_current_formatted_date_time(date_str):
    """
    Transform date string (from the linux command 'date')
    to a formatted date_time representation for difference check and comparison
    :param date_str:
    :return:
    """
    extracted_date = parent_datetime.datetime.strptime(date_str, "%a %b  %d %H:%M:%S %Z %Y")
    formatted_date = str(extracted_date.strftime('%Y-%m-%d %H:%M:%S'))

    return formatted_date


def get_current_formatted_date(date_str):
    """
    Transform date string (from the linux command 'date')
    to a formatted date
    :param date_str:
    :return:
    """
    extracted_date = parent_datetime.datetime.strptime(date_str, "%a %b  %d %H:%M:%S %Z %Y")
    formatted_date = str(extracted_date.strftime('%Y-%m-%d'))

    return formatted_date


def get_day_before_formatted_date(date_str, days_to_subtract):
    """
    Transform date string (from the linux command 'date')
    to a formatted date, and return the date subtracted by a given number of days
    :param date_str:
    :return:
    """
    extracted_date = parent_datetime.datetime.strptime(date_str, "%a %b  %d %H:%M:%S %Z %Y")
    yesterday = extracted_date - timedelta(days=days_to_subtract)

    formatted_yesterday = yesterday.strftime('%Y-%m-%d')

    return str(formatted_yesterday)


def get_date_diff_in_minutes(current_time, log_mark_time):
    """
    Get the difference between two dates in minutes.
    Time comparison requires time strings in a format of '%Y-%m-%d %H:%M:%S'
    :param log_line: [str] The log line from which to extract the dates
    :return: Integer indicating the difference between the dates in minutes
    """
    fmt = '%Y-%m-%d %H:%M:%S'

    d1 = datetime.strptime(current_time, fmt)
    d2 = datetime.strptime(log_mark_time, fmt)
    return (d1 - d2).seconds / 60


def get_test_result_by_bool(bool_val):
    if bool_val:
        return SUCCESS_MESSAGE

    return FAILURE_MESSAGE


def print_file_contents(file_to_print, msg):
    print msg
    with open(file_to_print, 'r') as fin:
        print fin.read()


def compare_files_write_diff(file1, file2, diff_file):
    t1 = open(file1).readlines()
    t2 = open(file2).readlines()

    myfile = open(diff_file, 'w')
    for line in difflib.unified_diff(t1, t2):
        if line[0] == '+' or line[0] == '-':
            myfile.write(line)

    myfile.close()


def general_multiline_interation_method(log_file, func, args):
    ret_val = True

    if is_empty_file(log_file):
        print ("\n" + log_file + " IS EMPTY !!!")
        return False

    with open(log_file) as f:
        for line in f:
            if func(line, *args) is False:
                print "First line with violation:  " + line
                ret_val = False
                break

        return ret_val


def is_number_in_range(num_to_check, op, compare_to):
    return op(num_to_check, compare_to)

