import pytest

def pytest_addoption(parser):
    parser.addoption(
        "--host_name", action="store", default="1.1.1.1", help="The machine on which to execute the tests"
    )
    parser.addoption(
        "--mag_pass", action="store", default="", help="Magnifier Password"
    )
    parser.addoption(
        "--run_id", action="store", default="", help="TestRail run id"
    )
    parser.addoption(
        "--tr_pass", action="store", default="", help="TestRail password"
    )


@pytest.fixture
def host_name(request):
    return request.config.getoption("--host_name")

@pytest.fixture
def mag_pass(request):
    return request.config.getoption("--mag_pass")

@pytest.fixture
def run_id(request):
    return request.config.getoption("--run_id")

@pytest.fixture
def tr_pass(request):
    return request.config.getoption("--tr_pass")


@pytest.fixture(scope="function", autouse=True)
def divider_function(request):
    print('\n        ---  %s start ---' % request.function.__name__)
    def fin():
            print('        --- %s finished ---' % request.function.__name__)
    request.addfinalizer(fin)