import sys
import os
import pytest

from utils import *
from test_helper import *
from tests_logic import *
from collect_data import collect_data
from tr_test_case import tr_test_case
from testrail import *

LC_USER = "lc"
CONFIG_FILE_NAME = "conf.json"
TRAFFIC_FLOWS_THRESHOLD = 0
CYCLE_DURATION_THRESHOLD = 600

MAGNIFIER_INFO_OUT = "MagnifierInfo.out"
BBR_STATUS_LOG = "bbr_status_log"
SERVICES_STATUS_LOG = "services_status.log"
SERVICES_COMPARE1 = "services_compare1.log"
SERVICES_COMPARE2 = "services_compare2.log"
INGESTER_SANITY_LOG = "kafka_progress_and_threads.log"

captured = None

@pytest.fixture(scope="session")
def configuration():
    configuration = load_config(CONFIG_FILE_NAME)

    configuration = prepare_config(configuration, get_arg_value(sys.argv[6]))
    host = get_arg_value(sys.argv[4])
    collect_data(host, LC_USER, get_arg_value(sys.argv[5]), configuration)

    return configuration


@pytest.fixture(scope="module")
def tr_client(configuration):
    client = APIClient(configuration["general"]["testrail_client_url"])
    client.user = configuration["general"]["testrail_client_user"]
    client.password = str(get_arg_value(sys.argv[7]))

    return client


def test_bbr_status(configuration, tr_client, capsys):
    tc = prepare_test(configuration, "92335")

    date_file = get_validated_log(configuration, MAGNIFIER_INFO_OUT)
    date = get_file_first_line(date_file).strip()
    bbr_log = get_validated_log(configuration, BBR_STATUS_LOG)

    bbr_last_registered_tik = check_bbr_last_close(bbr_log, date)
    print "Checking last close in BBR log is earlier than 11 minutes ago-  " + get_test_result_by_bool(bbr_last_registered_tik)

    tiks_close_time_ok = bbr_check_all_tiks_close_time(bbr_log)
    print "Checking last BBR log lines close time- " + get_test_result_by_bool(tiks_close_time_ok)

    handle_test_result(bbr_last_registered_tik and tiks_close_time_ok, tc, configuration, tr_client, capsys, "BBR tests resulted in failure")


def test_services_status(configuration, tr_client, capsys):
    tc = prepare_test(configuration, "92336")

    services_status_log = get_validated_log(configuration, SERVICES_STATUS_LOG)
    services_compare1 = get_validated_log(configuration, SERVICES_COMPARE1)
    services_compare2 = get_validated_log(configuration, SERVICES_COMPARE2)

    services_active = handle_services_status_failure(services_status_log)
    no_diff_in_time = handle_services_compare(services_compare1, services_compare2, configuration)

    handle_test_result(services_active and no_diff_in_time, tc, configuration, tr_client, capsys, "Magnifier Services are not active or shutting down")


def test_ingester_sanity(configuration, tr_client, capsys):
    tc = prepare_test(configuration, "92339")

    ingester_status_log = get_validated_log(configuration, INGESTER_SANITY_LOG)
    test_result = check_ingester_sanity(ingester_status_log)

    handle_test_result(test_result, tc, configuration, tr_client, capsys, "Ingester status is green")
