import time
import operator
from utils import *
from tr_test_case import tr_test_case
from testrail import *


MAGNIFIER_INFO_OUT = "MagnifierInfo.out"
GREEN_STATUS = "Green"


def get_test_case(conf):
    tc1 = tr_test_case(conf["general"]["magnifier_version"], conf["general"]["testrail_assignedto_id"])

    return tc1


def prepare_config(config, run_id):
    exec_dir = create_execution_dir()
    config["general"]["exec_dir"] = exec_dir

    config["general"]["run_id"] = run_id

    return config


def get_version(config):
    info_file = os.path.join(config["general"]["exec_dir"], MAGNIFIER_INFO_OUT)
    get_n_line_from_file(info_file, 3)


def set_case_result_to_testrail(tr_client, tc, config, test_result):
    """
    =========================   CHANGE THIS SAMPLE CODE  ! ! ! =========================
    :param tr_client:
    :param tc:
    :param config:
    :return:
    """
    if test_result:
        tc.status_id = 1
    else:
        tc.status_id = 5

    tc.version = config["general"]["magnifier_version"]

    post_url = 'add_result_for_case/{0}/{1}'.format(config["general"]["run_id"], config["case_params"]["case_id"])
    tr_client.send_post(post_url, json.loads(tc.toJSON()))
    time.sleep(1)


def handle_services_status_failure(log_file):
    bad_services_empty = is_empty_file(log_file)

    if not bad_services_empty:
        print_file_contents(log_file, "Some of magnifier services are not in active mode.")

    return bad_services_empty


def handle_services_compare(services1_file, services2_file, config):
    diff_file = os.path.join(config["general"]["exec_dir"], "diff.txt")
    compare_files_write_diff(services1_file, services2_file, diff_file)
    diff_empty = is_empty_file(diff_file)
    if not diff_empty:
        print_file_contents(diff_file, "There are differences in lc_status after 10 seconds")

    return diff_empty


def get_last_number_from_log_line(log_line):
    fields = log_line.split()

    t1 = (str(fields[-1:]).split(']')[0].split('=')[1])[1:]
    return float(t1)


def check_line_number_in_range(line, op, threshold):
    num_to_check = get_last_number_from_log_line(line)
    in_range = is_number_in_range(num_to_check, op, threshold)

    if not in_range:
        print "Threashold violation ! The violation line:"
        print line

    return in_range


def check_version_api(cmd_log, rest_log):
    rest_line = get_file_first_line(rest_log)
    cmd_line1 = get_file_first_line(cmd_log)
    cmd_line2 = get_n_line_from_file(cmd_log, 2)

    rest_base = rest_line.strip().split("-")[0]
    rest_build = rest_line.strip().split("-")[1]
    cmd_base = cmd_line1.split("=")[1].strip().strip("\"")
    cmd_build =cmd_line2.split("=")[1].strip().strip("\'")

    print "rest request base version:  " + rest_base
    print "rest request build number:  " + rest_build
    print "command base version:  " + rest_base
    print "command build number:  " + rest_build

    is_base_equal = (rest_base == cmd_base)
    is_build_equal = (rest_build == cmd_build)

    return is_base_equal and is_build_equal


def check_ingester_sanity(ingester_log):
    status_line = get_file_first_line(ingester_log)

    result = GREEN_STATUS in str(status_line)
    if not result:
        print_file_contents(ingester_log, "Ingester status is not 'Green'.\nIngester status command contents:")

    return result


def prepare_test(config, case_id):
    config["case_params"]["case_id"] = case_id
    test_case = get_test_case(config)

    return test_case


def handle_test_result(bool_outcome, test_case, conf, tr_client, capsys, msg_if_fails):
    global captured

    try:
        assert bool_outcome, msg_if_fails
        add_common_to_testcase(test_case, capsys)
        set_case_result_to_testrail(tr_client, test_case, conf, bool_outcome)
    except Exception as e:
        add_common_to_testcase(test_case, capsys)
        test_case.add_to_comment(e.message)
        print captured[1]
        test_case.add_to_comment("\n\nExecution Errors:\n" + captured[1])

        set_case_result_to_testrail(tr_client, test_case, conf, bool_outcome)
        raise e


def add_common_to_testcase(testcase, capsys):
    global captured

    captured = capsys.readouterr()
    testcase.add_to_comment(captured[0])


def get_version(info_log, config):
    ver_file = get_validated_log(config, info_log)
    version = get_n_line_from_file(ver_file, 3)

    config["general"]["magnifier_version"] = version
    return version